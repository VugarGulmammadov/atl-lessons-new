import jdk.nashorn.api.tree.Tree;

import java.util.*;
import java.util.function.Consumer;

public class Main {
    public static void main(String[] args) {


        }
    private static void enumSetOperations(){
        EnumSet<Name>enums=EnumSet.of(Name.George, Name.Jhon, Name.Nick, Name.Edward);
        enums.remove(Name.George);
        enums.forEach(System.out::println);


    }
    private static void hashSetOperations(){
        Set<String>set=new HashSet<>();
            HashSet<String>hashSet=new HashSet();

            hashSet.add("foo");
            hashSet.add("bar");
            hashSet.add("bar");
            hashSet.add("bar");
            int size=hashSet.size();
            boolean hashSetEmpty=hashSet.isEmpty();
            boolean foo=hashSet.remove("foo");
            boolean isDeleted=hashSet.contains("bar");
            Object[]objects=hashSet.toArray();
            hashSet.addAll(set);

        System.out.println("hashSet size is"+size);
        System.out.println("hashSet empty is"+hashSetEmpty);
        System.out.println("bar deleted is"+isDeleted);
        hashSet.forEach(System.out::println);
        hashSet.clear();

        }
        private static void LinkedHashSetOperations(){
            LinkedHashSet<String>LinkedHashSet=new LinkedHashSet<>();
            LinkedHashSet.add("foo");
            LinkedHashSet.add("bar");
            LinkedHashSet.add("hello");
            LinkedHashSet.forEach(System.out::println);
        }
        private static void treeSetOperations(){
            TreeSet<String>treeSet=new TreeSet<>();
            treeSet.add("India");
            treeSet.add("Australia");
            treeSet.add("South Africa");
            Iterator<String>ascendingItarator=treeSet.iterator();
            Iterator<String>descendingItarator=treeSet.descendingIterator();
            while(ascendingItarator.hasNext()){
                System.out.println(ascendingItarator.next());
            }
            while(descendingItarator.hasNext()){
                System.out.println(descendingItarator.next());
            }
        }
        private static void treeSetComparator(){
        TreeSet<Student>treeSet=new TreeSet<>(new StudentAgeComprator());
        PriorityQueue<Student>priority=new PriorityQueue<>(new StudentAgeComprator());
        treeSet.add(new Student(15));
        treeSet.add(new Student(16));
        treeSet.add(new Student(17));

        treeSet.forEach(System.out::println);
        treeSet.forEach(new Consumer<Student>() {
            @Override
            public void accept(Student x) {
                System.out.println(x);
            }
        });

            }
        }

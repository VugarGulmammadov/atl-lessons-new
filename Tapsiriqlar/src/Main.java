import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class Main {

    public static void main(String[] args) {
        //1.Write a Java program to add elements to the hash set
        //2.Write a Java program to convert a hash set to a List/ArrayList
        //3.Write a Java program to convert hash set to an array
        //4.Write a Java program to convert a hash set to a tree set
        //5.Write a Java program to compare two sets and retain elements from a hash set
        //6.Write a Java program to remove all of the elements from a hash set
        //7.Write a Java program to create a new tree set,add some colors (string)and print out the tree set
        //8.Write a Java program to iterate through all elements in a tree set
        //9.Write a Java program to get the number of the elements in a tree set
        //10.Write a Java program to compare two tree sets
        //11.Write a Java program to retrieve and remove the first element of a tree set
        //12.Write a Java program to retrieve and remove the first element of a tree set

        HashSet<String> set = new HashSet<>();

        set.add("Mouse");
        set.add("Keyboard");
        set.add("cable");
        set.add("Plugin");
        set.add("laptop");
        set.add("computer");


        convertingElements();
        convertingArray(set);
    }

    private static void convertingElements() {
        HashSet<String> set = new HashSet<>();
        set.add("Mouse");
        set.add("Keyboard");
        set.add("cable");
        set.add("Plugin");
        set.add("laptop");
        set.add("computer");

        ArrayList<String> list = new ArrayList<>();
        list.addAll(set);

        System.out.println("HashSet" + set);
        System.out.println("Arraylist" + list);

    }

    public static void convertingArray(HashSet<?> hashSet) {


       String arr[]=new String[hashSet.size()];
       HashSet.toArray(arr);
       for(String n:arr)
        System.out.println(n);




        }
    }





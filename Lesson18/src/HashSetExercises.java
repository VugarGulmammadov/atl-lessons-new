import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class HashSetExercises {
    //1.Write a Java program to add elements to the hash set
    //2.Write a Java program to convert a hash set to a List/ArrayList
    //3.Write a Java program to convert hash set to an array
    //4.Write a Java program to convert a hash set to a tree set
    //5.Write a Java program to compare two sets and retain elements from a hash set
    //6.Write a Java program to remove all of the elements from a hash set
    //7.Write a Java program to create a new tree set,add some colors (string)and print out the tree set
    //8.Write a Java program to iterate through all elements in a tree set
    //9.Write a Java program to get the number of the elements in a tree set
    //10.Write a Java program to compare two tree sets
    //11.Write a Java program to retrieve and remove the first element of a tree set
    //12.Write a Java program to retrieve and remove the first element of a tree set

    public static void main(String[] args) {
        HashSet<String> hashSet = new HashSet<>();
        hashSet.add("bmw");
        hashSet.add("mercedes");
        hashSet.add("audi");
        hashSet.add("wolksvagen");
        hashSet.add("audi");
        hashSet.add("Mazda");
        hashSet.add("Hyundai");
        hashSet.add("Porche");

        HashSet<String> hashSet2 = new HashSet<>();
        hashSet.add("bmw");
        hashSet.add("mercedes");
        hashSet.add("audi");
        hashSet.add("wolksvagen7");
        hashSet.add("audi");
        hashSet.add("Mazda");
        hashSet.add("Hyundai9");
        hashSet.add("Porche");


        System.out.println(hashSet);
        System.out.println("$$$$$");
        convertingArrayList(hashSet);
        System.out.println("''''''''''");
        convertingarray2(hashSet);
        System.out.println("/////////");
        System.out.println(compareSetsTest(hashSet,hashSet2));


    }

    public static void convertingArrayList(HashSet<String> hashSet) {
        ArrayList<String> array = new ArrayList<>();
        array.addAll(hashSet);
        System.out.println(array);
    }

    public static void convertingarray(HashSet<String> hashSet) {
        String[] array = new String[hashSet.size()];
        int i = 0;
        for (String c : hashSet) {


            i++;
            array[i] = c;
        }
        System.out.println(Arrays.toString(array));

    }

    public static void convertingarray2(HashSet<String> hashSet) {
        System.out.println("hashset contains=" + hashSet);
        String[] array = new String[hashSet.size()];
        hashSet.toArray(array);
        for (String n : array)
            System.out.println(n);

    }

   public static List<String> compareSetsTest(HashSet<String>masin,HashSet<String>masin2) {
       ArrayList<String> auto = new ArrayList<>();

       for(String n:masin){
           if(masin2.contains(n)){
               auto.add(n);

           }
       }
       return auto;

   }
}





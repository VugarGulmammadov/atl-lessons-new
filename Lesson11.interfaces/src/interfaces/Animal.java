package interfaces;

public class Animal implements AnimalActions, AnimalOtherActions {



    @Override
    public void breathe() {
        System.out.println("interfaces.Animal is breathing");
    }

    @Override
    public void run() {
        System.out.println("interfaces.Animal is running");

    }

    @Override
    public void sleep(String name) {
        AnimalActions.super.sleep(name);
    }
}

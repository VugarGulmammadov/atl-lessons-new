import java.util.Scanner;

public class IsNumberinsideArray {
    public static void main(String[] args) {
        int[] array = {2, 45, 3, 78, 90, 12, -5, 0, 33};

        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();

        for (int n : array) {
            if (n == number) {
                System.out.println("Number exists in array");
                break;
            } else {
                System.out.println("Number does not exist");
            }
        }
    }
}




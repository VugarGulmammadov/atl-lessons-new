package person;

import java.util.Objects;

public class Person {
    private int age;
    private String address;
    public Person(int age,String address){
        this.age=age;
        this.address=address;


    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age && address.equals(person.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(age, address);
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", address='" + address + '\'' +
                '}';
    }
}

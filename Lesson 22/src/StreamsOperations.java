import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class StreamsOperations {

    public static void main(String[] args) {
        IntStream intStream=IntStream.range(0,100);
        LongStream doublestream=LongStream.range(0,100);
        intStream.forEach(System.out::println);

   //     generatingRandomNumbers(10,10,100);
        int[]ints=new Random().ints(10,1,100).toArray();
        for (int i=0;i<ints.length;i++){
            System.out.println(ints[i]+" ");

        }


    }
    private static void print(String...strings){
        Arrays.stream(strings).forEach(System.out::println);
    }
    private static void generatingRandomNumbers(Integer size,Integer min,Integer max){
        new Random().ints(size,min,max).forEach(System.out::println);
    }
}
